# Introduction
This paste goes through my 5 year experience of developing the plugin, which started in 2016. It explains how it all started, what I went through, how I got to this point and why I don't want to do this anymore. It contains behind-the-scenes information and experience which end users have no idea about and most of them don't even care about.

# How it all began
I will not go through my whole minecraft history, I will start at the point where I decided to make my own plugins for my server. After I decided to close my last server, which was in 2016, I was left with plugin development knowledge, which I wanted to use in some way.

Back then bot attacks were a popular thing where I lived and no public plugins offered satisfying protection. I went through all available anti-bot plugins and took the best out of them. Then I decided to upload it as a paid plugin to spigot. One of the requirements is to have 3 free plugins. For that reason, I made 3 random small plugins (one of them literally had 7 lines), TAB being one of them. At that time it only offered header/footer, which I took from a tutorial and tablist name formatting. My paid plugin request was declined without any feedback (doesn't really surprise me).

I was then left with 3 small public plugins. TAB was the most prominent one, with frequent feedback from users, asking me to add new functions or occasionally reporting a bug. This is basically what I've been doing to this day.

# The rise
The plugin had a massive advantage over other plugins of it's kind - activity. I was adding requested features, barely saying no, fixing reported bugs and releasing updates every week. Other plugins were and still are [abandoned with no support, bug fixes and 200+ open github issues](https://github.com/sgtcaze/NametagEdit/issues). Already outperforming the competition didn't stop me from improving the plugin, which gave me an enormous lead which no one could match.
The plugin was also free, [which is by far the most important aspect of any plugin](https://www.mc-market.org/resources/20631/reviews#resource-review-25500). It even offered more than paid plugins, which ended up losing new customers rapidly, as well as [some existing ones switching to my free alternative](https://media.discordapp.net/attachments/959376301511282709/959376316942147584/unknown.png).

# Open source
Throughout the plugin's existence I had [many](https://media.discordapp.net/attachments/959376301511282709/959376544894156850/unknown.png) [users](https://media.discordapp.net/attachments/959376301511282709/959376595842375700/unknown.png) requesting me to make the plugin open-source. However, none of them could tell me how would I benefit from it. They just kept saying "it will be better" or "it will allow others to contribute" - theoretically it does allow people to contribute, but it doesn't mean they will. 
Look at the pull requests I got so far - majority of them [do absolutely nothing or were a mistake](https://github.com/NEZNAMY/TAB/pull/396/files). The rest comes from a person who is helper on the discord, who I could just send the source to if he asked. Yes, I am the one who takes the credit in the end, which is why I perfectly understand it, but it still doesn't change the fact that I was lied to. 
Eventually I had enough of the requests and just like in every other scenario I did what people asked me to, so they shut up and the opensource flag changes from false to true and whenever someone asks anywhere whether the plugin is opensource or not, the answer can change from "no" to "yes".

It doesn't end here. There's a saying: "Fix one bug, you'll get two more". This was the case as well. Problem of people saying "the plugin is sadly not opensource" was solved, but it came at a price. [Instead, people started flaming me all they could for anything they could](https://gist.github.com/NEZNAMY/21c1aabe57a0a462ee175386c510fdf8#their-behavior). First, [people didn't like that from the beginning I was using export project function from eclipse, instead of "a proper build system"](https://media.discordapp.net/attachments/961146779846451262/961147079093284894/unknown.png?width=655&height=668)). I did that, I switched to maven. Then, they didn't like that I was not using modules to separate bukkit/bungeecord/share packages. I did that, it was a pain with no examples of combining them all into one jar, but I eventually figured it out and did it. Then, people started saying I should switch to gradle, because "it's faster", even sending links of someone making a maven vs gradle blog. In this case, for the first (and last) time ever the plugin got an actual contribution from someone else, who did this for me. The argument of gradle being faster was a pure lie. Instead of the plugin taking 15 seconds to compile, it takes 1 minute now. Done? Of course not. Code compilation would be done, but there's one more issue - the code itself. 100 developers = 100 ways of coding. [People started saying the don't like my code structure (this one was a paper dev btw)](https://media.discordapp.net/attachments/959376301511282709/959385191300866048/unknown.png) and so on, possibly even saying it's an amateur work. Well yes, if I was a professional, I wouldn't give one fuck about a block game. This is practically a never-ending story as I can never satisfy everyone with this one.

# Premium version
When the plugin started geting bigger, I got recommendations to make the plugin paid. 
There were many problems with that: 
* I remembered how my previous paid plugin attempt got declined without providing a reason
* I had nothing extra to offer in a premium version, since all features I had were already included in the free one
* Making the plugin premium-only would stun it's growth potential and make users simply switch to other free plugins
* I didn't think it would make any difference, since donations were open already and anyone who wanted to support me already did
* The plugin was opensource, doesn't that make the premium version idea useless?

Despite that I got an interest in this. Working on the plugin was my hobby and getting paid for doing something you enjoy doing is a dream job. It would also serve as an alternative to a part-time job, which some of my friends had already.

Eventually, I gathered a few bonus features from user requests, as well as some ideas of my own and decided to make those as a part of a separate - premium version. I solved the spigot requirement issue (yes, I was still not meeting their requirements) / plugin denial by uploading the plugin to MC-Market instead. For opensource issue, I saw big plugins with thousands of buyers having a free download and being opensource - perhaps this is the superior option with all the people appreciating it and buying in return? After 2 years I can conclude that the answer is no.

# First problems
After introducing the premium version and people actually started buying it, my life felt like [Pink fluffy unicorns dancing on rainbows](https://www.youtube.com/watch?v=qRC4Vk6kisY). It didn't last for long. At that point, plugin had a free and premium version - with support for premium version only being available to buyers. However, since the plugin was opensource as well as websites like blackspigot exit, anyone could get the premium version without paying with little effort. This defeated the purpose of premium version's existence, since it really only was an alternative button for donations. Not just that, but we also got people arrogant enough to ask for help with premium version without buying it. Some argumented that they compiled it themselves despite readme saying they won't get help with self-compiled jars, some said they are just trying the plugin out before buying - which is what the free version was for. [Some even argued with me trying to make me reponsible for helping everyone with the premium version, even those who didn't buy it, because "it's opensource anyway"](https://support-system.xyz/redirect?link=https://cdn.discordapp.com/attachments/690574378454483034/875133319132622878/ticket-868945853581234176.html).

# Toxic community
Friendly behavior doesn't spread, toxicity does very well. Disrespecting others' work is nothing new, but that doesn't mean it's okay. Instead of playing games in my free time, I am working on a free opensource plugin. [The first major outbreak was on paper discord, when the plugin broke on 1.14.4 when it came out, because it only supported up to 1.14.3](https://discord.com/channels/289587909051416579/289587909051416579/602040164457644047). Large NMS changes yet not bumping server package caused the plugin to load on an unsupported server version, causing entities to be invisible. I fixed the compatibility issue within hours, yet people kept actively shitting on the plugin for months since that incident wherever they can. It hasn't stopped to this day. Wherever I step (paper discord, [old velocity discord](https://gist.github.com/NEZNAMY/21c1aabe57a0a462ee175386c510fdf8#their-behavior), [kyori](https://gist.github.com/NEZNAMY/21c1aabe57a0a462ee175386c510fdf8#bug-report-feedback), other), I am being harassed. Hell, every my users got harassed by paper devs for using the plugin! Someone even told me they received death threats, though I lost evidence for this. Toxic environment is not healthy and people usually quit their jobs in such case. Why should I stay here? For what?

# (Ab)users part 1
Large free plugins became a standard in the minecraft server community, which now takes plugins for granted. Users don't care how much time and effort the plugins took to make, they only care about the final free product that they can install on their commercial server and make money.

Support has already changed a lot over the years. Back when I was running a server, I would never even think of asking the authors to help me configure their plugins. Wikis existed. Yes, no one can be good at everything. For that reason we hired people we called "plugin masters", who had knowledge about plugins and configured them for us. This should still be a thing by the way. Server owners should not be the configuring plugins. Especially not the 10 year old ones.

Discord has changed everything. Discord is a great tool with great intention, which brought plugin developers closer to their users. Sadly, this is being heavily abused. Users pinging plugin authors and demanding instant attention with their problem answered in the FAQ is a daily thing. Pointing this out and asking users to not ping staff members only makes them even more aggressive.

I had free support and even a ticket system available to everyone, which many users took as an invite to abuse them. Asking me to explain some functionality to them and getting a response "I didn't understand it" after receiving wiki link, requesting a rephrase of the whole wiki and then requesting me to configure the plugin for them became standard. Lots of inactive tickets, users being aggressive when pinged after a week of inactivity creating a mess for the staff members, some even admitting they shut their server down in the meantime. The worst part of this is that vast majority of servers won't last for longer than 1 month, so all that effort goes to waste. Users say they don't need scoreboard feature when someone brought the premium version up to the discussion.

# Changing monetization model to paid support
Full explanation behind this decision can be found in 3.0.0 changelog. The most important parts are explained above - free users taking a lot of my time and after I help them, they get the premium version from blackspigot or compile themselves and more. Another server using the plugin doesn't take my time, another user asking for help does. When I brought this idea up for the first time, feedback was very positive - who would complain about premium functions becoming free? About the paid support part, free users just said it's fine as they won't need help anymore. In other words, "Alright, I already abused the shit out of this person by taking up their time, now I will use previously paid functions for free". In the end, they all asked for help sooner or later ...

# (Ab)users part 2
Free plugin with paid support is a model which is still very new to the minecraft community. I was expecting users to be more respectable, considering they have the plugin available for free now. It's exactly the opposite. Community support as the only support-related channel didn't make people realize I was serious, in fact, they took it as an invite to ask for help in private messages. When I explain the situation, they either get aggressive, say things like "lol why should I pay for support" or "please it's just this one thing". 
Most of the time the answer is directly on the wiki. Do I send them the link? Does that count as help? If not, what possibly would? If yes, should I start a long discussion instead of spending 20 seconds by searching up wiki link that exactly answers them? Same thing for community support channel. People pinging me for help, with the rest of the discussion being same as in private messages explained above. Or, not pinging me and not getting any help and then complaining how terrible the support is.

# It's not what it used to be
Originally, the plugin started as a hobby. Writing some code, adding new features, doing things others considered impossible, occasionally fixing a bug and helping those few users with configuration and enjoying the view of a working plugin being used in production.
However, things changed completely. 

Demand from users is very high, asking for all features they can possibly imagine. Satisfying them means a lot of time involved.

"Simple bugs" were fixed and any incoming bug reports are very hard to reproduce even for users themselves, and completely impossible for me to reproduce. Steps to reproduce are obviously never present, which ends up taking hours of my time for every bug report and asking all questions I can think of, quite often not coming to anything I could use to fix it.

Providing support started taking a lot more time as well. The more information I put on the wiki, the more trivial questions people are asking. Even when receiving a direct wiki link answering their question, their reaction is often "didn't work", "I didn't understand it" or "can you explain it to me?", as if I was the wiki myself. Instead of users trying to figure out things themselves and often successfully, they now use support for any issue they run into, without even trying first. Often I see users saying "I solved it" about 30 minutes after their question, proving that they don't really need help. Instead of being thankful for any attention they get, they keep asking more until we rephase the wiki to them, and when we refuse, they get aggressive.

All of this lead to a significantly higher amount of time needed to satisfy the demand. I had a choice - go the way of keeping it as a hobby and do whatever I want, whenever I want and keep it free, or start monetizing the time I am putting into the plugin. Considering there already is enough of broken outdated abandoned spigot plugins and TAB would just become another one of them, I decided to go with the second option. Checking tickets before going to sleep, as well as grabbing phone and checkig them again when I wake up has become an everyday routine. Coding itself has become like 25% of the work on the plugin, instead of original 90%+.

# Fatal mistakes - there's nowhere to go now
Life is not a school test with 1 correct answer and others being wrong. Very often no answer is correct and the one that's the least bad is chosen. Identifying this option is not easy and I failed at this as well.
There are many mistakes I made, both short and long term, which cannot be recovered from.

My biggest long-term mistake was not clearly setting my goal(s) with the plugin, instead, I was trying to do "everything":
* I wanted the plugin to be as good as possible
* I wanted the plugin to become a standard and be the go-to plugin for every server, including new servers
* I wanted to get an evaluation of my time put into the plugin in form of an income
* I wanted to keep the plugin as a free-time activity, not a full-time unpaid job
* I wanted users to be happy with the plugin

All of these sound fair at first. However, if you look closer, many of these are mutually incompatible by definition:
* You can't make someone happy and make money off of it. Happiest customer is the one that doesn't even need to pay.
* You can't keep something as a free-time activity and keep users happy and add everything in real time.
* You can't get a plugin to become standard if it's paid.

And so on. These goals clash with each other, until one of them takes over or none of them is truly met. The latter is the case here:
* I started declining all incoming feature requests
* The plugin's growth is [stunned](https://bstats.org/plugin/bukkit/TAB%20Reborn/5304). It already is at its peak and won't get any more popular due to many factors.
* Amount of people respecting my decision of paid support is low and the amount of purchases is drastically decreasing.
* Despite all of this, I spend hours every single day on the plugin in some way.
* Users will always find something to complain about - support being paid the most common one since the change.

Another fatal mistake turned out to be making support for buyers only. This bad decision was made due to another bad decision from the past - having the plugin opensource while trying to sell the plugin - that obviously won't work. Going opensource sentenced me to very limited income due to no one paying if they don't need to. As well all know, donation buttons are useless since no one is using them.
Unlike previously, when free users got help, they sometimes said they will consider buying the plugin in return. Most of them never did, but there's always a chance. Now, they believe support should be free and will refuse to pay for it.

In terms of short-term mistakes, reputation takes years to build, seconds to ruin. Affected reactions to stupid people led to people completely changing their view on the plugin due to one thing I said, spreading this everywhere they could. In combination with random hate for the plugin itself without reasons, this lead to me not being present in large plugin-related discord servers.

All my previous actions and decisions put me into a very tough spot. Buyers were promised support and ability to request features. Switch to paid support made some people ask for a refund, despite getting help many times in the past. Some people got a great idea of buying a copy of the plugin and request new and new features instead of hiring a developer, because it will be a lot cheaper this way. The rest took it as an invite to abuse support and ask every possible question in existence. We have users who had 20+ tickets in the past, but they still keep going to this day.

Do I limit the amount of time spent on the plugin? How do I do that? Do I close feature requests despite it being an advertised advantage for buyers? Do I stop helping users who literally paid for it? Do I grab money for support but let someone else provide it without being paid? Paying them? That would drop the income to 0. When a free user asks for help in private messages or ping me in community support, do I take 10 seconds to find the wiki link and break the promise of not helping free users, taking even that advantage from buyers? If not, do I start a long argument that's going to take a ton more of my time and instead of making user understand my situation get them to send messages like [this one](https://media.discordapp.net/attachments/659545536533364776/948973234764775486/unknown.png?width=1214&height=332)? Similar for questions in community support with very simple answer / answer on the wiki - do I ignore them or take 10 seconds to answer?
All of this causes higher and higher demand from the community, while income going down rapidly. The worst part is that I cannot even quit with respect - how about all the buyers? Do I refund them and completely nullify all income I ever had? Do I leave them paying for something they won't get?

# Is there anything I can do about this as the author?
The problem is that I don't think my time is fairly evaluated. There are two possible solutions - increase income or reduce time put into the plugin. Increasing income is out of my control. Any possible solutions would only make things worse:  
* Reverting back to free/premium would require another config rework, which people won't be happy to see. Most of them would simply not update or just use blackspigot / compile plugin from source and demand support for free
* Closing source and going premium only would, just like in the case above, result in users not updating and only adding new MC version support to old source, bypassing new system entirely. Plus leaking websites.
* Any attempts at advertisement would definitely cost more than the money they would bring

Because of this, the only way I can balance this situation is by reducing my activity, which is the option I am going for.

# What's next
I must establish the plugin's place in my life. If it's supposed to be a hobby, I should work on the plugin whenever I want as long as I want. However, that's not really the case anymore, due to increasing demand from users, especially after paying for support, which they should get when they paid. Many users ping me immediately when they need help, not considering if I'm available or not. Some provide very limited information about a bug and ask me every day when will it be fixed, so they can put it on their server and make money off of it.  

If it's supposed to be a job (or a part-time job in my case, since I still study), I expect to be paid for my work, otherwise I may as well just get an actual part-time job where I will get paid regularly, not only when I win an argument over someone who believes my free time should be available to everyone for free.  

Previously, I was working on the plugin on my own, allowing users to request new features. After that, I got feedback from them in form of donations or purchasing a copy of the plugin.  

Now, I will turn this and my work on the plugin will be a feedback to users. This might be the first case when plugin's future is fully in users' hands by funding any future development, not just "add this, fix that". If it goes well, I will open feature requests again and will proceed to work on the plugin as I did before. If the community decides this should not be a job of any kind, I will respect that, but the community must as well.  

**If you wish to donate, you can buy the plugin on [MC-Market](https://www.mc-market.org/resources/14009/) or [send a donation](https://paypal.me/neznamy1). If the community funds the development, I will keep improving the plugin. The plugin's future is in YOUR hands as well.**

# Clarifications
Before spreading any lies or believing those being told you, let's clean some things up:
* Support **will** still be provided to buyers just like before
* Reported bugs **will** be fixed
* The plugin **will** get updated for new MC versions
* The plugin is, and **will** still be available to download for free
* The plugin is **not** going abandoned, it is just in the hands of users now (let's hope it's not the same)
* Feature requests **will** be open again if the community decides to fund the development
* Instead of working on the plugin and then hoping for people buying it, work will be put into the plugin after funds are raised

# FAQ
This section will answer common questions / react to feedback from users regarding this paste.

## Why not creating a Patreon account?

### It's just another way of donating
Patreon is just an alternate way of donating. It doesn't allow anything casual donations wouldn't. Anyone interested to donate, can do so directly. Both one-time and manually donating every month are available options. To not need to bother every month with manual donations, you can donate annually. Or just donate it all "at once". Who wants to donate can do so even without patreon.

### Goals
When creating fundraising like this, donation goals are expected. The problem is, I don't have one goal. The plugin is not like a line that's getting longer, it's like a circle. Providing support, fixing bugs/errors, adding new features, improving wiki, etc etc. My goal cannot be "once we reach X$/month, I will provide support to those who already paid for it", "I will fix reported bugs" or "I may or may not implement requested features". That just sounds terrible.

### Tiers and advantages
Currently, everyone has full access to years of my work for free. Purchasing the plugin gives access to my personal time with configuration of the plugin for your server. There is nothing more I could possibly offer to subscribers (requesting features is a bad idea, since me declining them makes people pay for nothing and if I accept, they save a ton of money hiring a dev to make a private plugin for them). This would require yet another distribution change, which I don't even want to do. Restricting free users would result in them either not updating, using leaks or just switching to other plugins. 


Changing distribution again or any similar actions to artificially give me work would definitely require more time and effort being put into the plugin and maintaining it, but not bring as much funds, further escalating the problem of insufficient time evaluation.