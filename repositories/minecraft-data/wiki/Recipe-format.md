The recipe format, as [currently used in mineflayer](https://github.com/andrewrk/mineflayer/blob/c471b5f06fd4a5c8b07bd92e235cf02729bc9dd0/lib/enums/recipes.json).

```js
{ // dictionary of ID -> recipes for that ID
	"<result ID>": [ // list of recipes
		{ // one recipe to get an item with ID <result ID>
			"type": "<result ID>"
			"metadata": "<result damage>",
			"count": "<result amount>",
			"inShape": [ // the crafting grid, list of rows
				[ // one row of the crafting grid
					"<A1>",
					"<A2>",
					"<A3>"
				], [
					"<B1>",
					"<B2>",
					"<B3>"
				], [
					"<C1>",
					"<C2>",
					"<C3>"
				]
			],
			"outShape": ["..."] // optional, same as inShape,
			                    // but the items that remain
			                    // in the grid after crafting
		},
		{ // another recipe for the same ID, but maybe different meta
			"type": "<result ID>"
			"metadata": "<result damage>",
			"count": "<result amount>",
			"ingredients": [ // list of ingredients
				{
					"id": "<ingredient ID>",
					"metadata": "<ingredient damage>" // optional
				}
			],
		},
		{ // another recipe for the same ID, may be for different meta
		// ...
		}
	],
}
```

The crafting grid looks like this:

    A1 A2 A3
    B1 B2 B3
    C1 C2 C3

When the required grid is smaller, empty rows/cells may be omitted:

    A1 A2

would be `"inShape": [["A1", "A2"]]`,

    A1
    B1

would be `"inShape": [["A1"], ["B1"]]`,

The numbers (`A1` to `C3`) in `inShape` are the required item's ID, or `null` if empty.

`result ID` is duplicated for [convenience](https://github.com/andrewrk/mineflayer/blob/c471b5f06fd4a5c8b07bd92e235cf02729bc9dd0/lib/recipe.js#L95).

## Json schema
The [json schema](https://github.com/PrismarineJS/minecraft-data/blob/0f5f93e79b447e6d778d50dd68aae0f31f0e7b6d/enums_schemas/recipes_schema.json) automatically checks whether the recipes.json respects that format.
