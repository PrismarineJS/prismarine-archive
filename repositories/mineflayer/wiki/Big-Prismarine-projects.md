Here are important tasks that would make PrismarineJS go one level beyond


Want to do something ? go talk about it in the issues or in advanced coders room at gitter or discord
also see [https://github.com/orgs/PrismarineJS/projects/9](https://github.com/orgs/PrismarineJS/projects/9)
also go to [https://github.com/PrismarineJS/mineflayer/issues/974](https://github.com/PrismarineJS/mineflayer/issues/974) to talk about it

## AI
* one of the original goal of mineflayer, and still a very interesting usecase today was to make possible a bot that can solve the game, can we do this as part of https://github.com/PrismarineJS/mineflayer/issues/981 ?

## visualization
* improve on top of https://github.com/PrismarineJS/prismarine-viewer

## easier maintenance
* better procedure to update mcdata, see [PrismarineJS/minecraft-data#283](https://github.com/PrismarineJS/minecraft-data/issues/283)

## more protocol support
* bedrock is the future of minecraft, let's have support for it [PrismarineJS/node-minecraft-protocol#235](https://github.com/PrismarineJS/node-minecraft-protocol/issues/235)

## more doc => more users and less questions
* we need tutorials for mineflayer [#908](https://github.com/PrismarineJS/mineflayer/issues/908) one form this could also take is a blog post / reddit posts to increase mineflayer popularity even more

## more libs => less duplicated code and more users
* make a generic proxy lib on top of node-minecraft-protocol for fun and profit https://github.com/PrismarineJS/node-minecraft-protocol/issues/712

