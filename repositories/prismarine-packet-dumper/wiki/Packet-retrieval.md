| Status | Packet name | How to get | Difficulty |
| --- | --- | --- | --- |
| ⬜️ | spawn_entity_experience_orb | kill an entity | easy |
| ⬜️ | spawn_entity_painting | place a painting | easy |
| ☑️ | named_entity_spawn | have another bot tp into range of recording bot | very easy |
| ⬜️ | [animation](https://wiki.vg/Protocol#Entity_Animation_.28clientbound.29) | take damage | easy |
| ⬜️ | statistics | take damage | easy |
| ⬜️ | block_break_animation | spawn another bot and have them mine | medium |
| ⬜️ | tile_entity_data | place a bed | easy |
| ⬜️ | [block_action](https://wiki.vg/Protocol#Block_Action) | open a chest | medium |
| ⬜️ | boss_bar | spawn a wither | easy |
| ☑️ | tab_complete | [bot.tabComplete](https://github.com/PrismarineJS/mineflayer/blob/master/docs/api.md#bottabcompletestr-cb-assumecommand-sendblockinsight) | very easy |
| ⬜️ | [face_player](https://wiki.vg/Protocol#Face_Player) | run /tp with the facing option set to true | hard |
| ⬜️ | nbt_query_response | open a chest | medium |
| ☑️ | chat | bot.chat | very easy |
| ⬜️ | transaction | take something from a chest | medium |
| ⬜️ | close_window | close a chest | medium |
| ⬜️ | open_window | open a chest | easy |
| ⬜️ | craft_progress_bar | use a furnace | medium |
| ☑️ | set_cooldown | throw an enderpearl | medium |
| ☑️ | named_sound_effect | walk / run | easy |
| ⬜️ | kick_disconnect | kick bot | very easy |
| ⬜️ | explosion | set off tnt | easy |
| ⬜️ | unload_chunk | walk out of a chunk's render distance | medium |
| ☑️ | game_state_change | start raining | very easy |
| ⬜️ | open_horse_window | open horse inventory | hard |
| ⬜️ | world_particles | make a nether portal | easy |
| ☑️ | map | use a map | easy |
| ⬜️ | trade_list | use a villager | medium |
| ⬜️ | entity | spawn a player that doesn't move for a tick | very easy |
| ⬜️ | vehicle_move | use a boat | hard |
| ⬜️ | open_book | open a signed book | easy |
| ⬜️ | open_sign_entity | place a sign | easy |
| ⬜️ | craft_recipe_response | according to [this](https://wiki.vg/Protocol#Craft_Recipe_Request), click in crafting book | hard |
| ⬜️ | [combat_event](https://wiki.vg/Protocol#Combat_Event) | not really possible anymore | impossible |
| ☑️ | remove_entity_effect | give a potion for 1s and wait for it to expire | very easy |
| ⬜️ | resource_pack_send | force server to send a texture pack | hard |
| ☑️ | respawn | kill bot | very easy |
| ⬜️ | camera | click an entity from spectator mode | hard |
| ⬜️ | [update_view_distance](https://wiki.vg/Protocol#Update_View_Distance) | not sent | impossible |
| ☑️ | scoreboard_display_objective | make scoreboard show | medium |
| ⬜️ | attach_entity | leash a horse to another horse | hard |
| ☑️ | scoreboard_objective | add another scoreboard objective | medium |
| ⬜️ | set_passengers | enter boat | hard |
| ⬜️ | teams | make a team | hard |
| ☑️ | scoreboard_score | update scoreboard score | medium |
| ☑️ | [title](https://wiki.vg/Protocol#Title) | make a message above hotbar or display in middle of screen | hard |
| ⬜️ | entity_sound_effect | take drowning damage | easy |
| ⬜️ | stop_sound | stop rain | very easy |
| ⬜️ | playerlist_header | never sent by vanilla server | impossible |
| ⬜️ | collect | pickup an item or xp orb on ground (break chest with items in it, and let items fall) | medium |
| ☑️ | entity_effect | drink a potion / set effect with command | very easy |
| ⬜️ | select_advancement_tab | ? | hard |
| ⬜️ | acknowledge_player_digging | bot.dig | easy |