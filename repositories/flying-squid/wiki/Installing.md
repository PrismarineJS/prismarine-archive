## Requirements

To install Flying Squid, you need:
* [NodeJS](https://nodejs.org/en/)

## Installing

Once you've installed NodeJS, download the Flying Squid source code from [here](https://github.com/PrismarineJS/flying-squid/releases/).
Then, unzip it into your server folder and open your terminal in that folder.

To install all the packages, run the following command.
```
npm install
```

To run the server, run this command:
```
node app.js
```