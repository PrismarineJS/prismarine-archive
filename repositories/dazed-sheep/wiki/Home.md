## Welcome to the dazed-sheep wiki!

To get started, read any of the following pages:

- [Installation](https://github.com/dazed-sheep/dazed-sheep/wiki/Installation)
- [Writing Plugins](https://github.com/dazed-sheep/dazed-sheep/wiki/Writing%20Plugins)
- [Installing Plugins](https://github.com/dazed-sheep/dazed-sheep/wiki/Installing%20Plugins)
- [World Format](https://github.com/dazed-sheep/dazed-sheep/wiki/World%20Format)

If you experience any problems on this wiki or any problems encountered while using dazed-sheep, please create an issue on our [issue tracker](https://github.com/dazed-sheep/dazed-sheep/issues).