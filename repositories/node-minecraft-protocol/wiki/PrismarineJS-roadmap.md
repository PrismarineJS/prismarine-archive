Objectives of this page : explaining the state of prismarinejs : here's what's happened so far, here's where were trying to go, this works, this doesn't work...

1. andrewk made mineflayer to create bots in c++ using js plugins
2. he then rewrote the network core in js as node-minecraft-protocol
3. then he rewrote mineflayer using nmp
4. rom1504 extracted minecraft-data from mineflayer to be a language independant store of minecraft data for mineflayer and others
5. flying-squid was made : a js minecraft server based on nmp and minecraft data
6. modules were extracted out of mineflayer to put into common with flying-squid (prismarine-block/recipe/biome/etc)
7. minecraft-data included data for several versions to make multiprotocol easier
8. protodef was extracted from node-minecraft-protocol : a generic parser/serializer working with json protocol definitions
9. node-minecraft-protocol became multi-version soon followed by mineflayer.

Other stuff:
* protodef is independant from js, so a compiler was made by hansihe to implement stuff in other languages
* minecraft-data was used by other languages (java, haskell, python)
* various path were followed for mineflayer to make smart bot (path finding, task scheduling, etc)
* flying-squid multiversion is current WIP and should follow the same path as mineflayer

future plans can include
* using the protodef compiler in nmp
* exploding mineflayer/flying-squid in more modules to be able to build other stuff (powerful proxies for example)
* lots of AI can be done with mineflayer


This is missing a lof of details and links and can be completed