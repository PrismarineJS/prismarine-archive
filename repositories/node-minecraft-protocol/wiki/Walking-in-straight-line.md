# WalkerBot

This is a dumb little bot that walks forward in a line on the X axis. Note that it does so without checking if it is actually walking on the ground, so it might float. It might also run into a block, in which case the server will either push him back, OR... make it suffocate

# Caveats

- Sometimes suffocates when running into a block/tree for some reason.
- Doesn't "fall down" when there is no block under him.

# Code

```javascript
	

    //var mc = require('minecraft-protocol');
    var mc = require('../');
    var client = mc.createClient({
      host: "cmc.im",
      port: 25565,
      username: "yourusername",
      password: "yourpassword",
    });
     
    function x() {
        position.x += 0.26;
        refreshposition();
    }
     
    var currTimeout = null;
    function startTimeout() {
      if (currTimeout == null) {
        console.log("Starting");
        currTimeout = setInterval(x, 50);
      } else {
        console.log("Already started");
      }
    }
     
    function stopTimeout() {
      if (currTimeout != null) {
        console.log("Stopping");
        clearTimeout(currTimeout);
        currTimeout = null;
      } else {
        console.log("Already stopped");
      }
    }
     
    client.on('connect',function() {
      console.log('Stuff to do when you recieve the connect packet!')
    });
     
    client.on('chat', function(packet) {
         console.log(JSON.stringify(packet.message));
         if (packet.message.indexOf("stop") > -1)
           stopTimeout();
         if (packet.message.indexOf("start") > -1)
           startTimeout();
    });
     
    var position = {
      x: 0,
      y: 0,
      z: 0,
    };
     
    client.on('position', function(packet) {
      console.log(packet);
      position.x = packet.x;
      position.y = packet.y;
      position.z = packet.z;
      refreshposition();
      startTimeout();
    });
     
    function refreshposition()
    {
      if (client.state === "play") {
        //console.log(position);
        client.write('position', {
            x: position.x,
            stance: position.y - 1.62,
            y: position.y,
            z: position.z,
            onGround: false
        });
      }
    }
```