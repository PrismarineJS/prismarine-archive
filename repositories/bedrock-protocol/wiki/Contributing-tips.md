As put in - https://github.com/PrismarineJS/mineflayer#modules - the [Node Way](https://amanhimself.dev/blog/the-node-way/),
> "When applications are done well, they are just the really application-specific, brackish residue that can't be so easily abstracted away. All the nice, reusable components sublimate away onto github and npm where everybody can collaborate to advance the commons." — substack from ["how I write modules"](https://pinecone-forest.com/lisplang-v0.0.142/Writing-Modules.html)

We want small composable packages that are only good at doing one thing well. If something can be a standalone package, it probably should be. It makes things easier to test and reuse in other projects while providing meaningful abstraction. Which in turn, makes code easier to read and write.

## Code

Do:
* Simplify the code (delete unnecessary parts of code, refactor code to use simple logic, try to be terse with meaningful variables)
* Use packages to separate parts of code that could be an independent unit or usable in other projects
* Write idiomatic JavaScript (write code that looks like normal, easy to read JavaScript code)
* Comment the code
* Document any API changes
* Ensure tests cover your changes

Avoid:
* Using large functional programming one-liners (split it out or use imperative) 
* Creating small 1 or 2 line functions only called once (inline them)
* Meaningless variable names, or variable names too long
* Over-engineering (doing much more than needed to solve problem)
  * Splitting your code into many files, objects or classes unnecessarily
* Writing slow code (should be reasonably fast)
* Writing hacky code

## Docs

Do:
* Cover all exposed API
* Use simple words, use less words (be terse)
* Provide code samples

Avoid:
* Over explaining (assume user knows basic coding and what lib is about)
* Writing full programs in docs