bedrock-protocol is a protocol library that allows nodejs programs to easily communicate with Minecraft servers and clients. The library provides a layer of abstraction ontop of the existing network protocol used for communication in Minecraft (Raknet/UDP). 

The library allows you to easily send and receive packets in JSON, authenticate clients and even handles some of the minimal boilerplate to get a client or server fully connected. Everything after the initial login, such as sending chunks or resource packs however is the responsibility of downstream programs to implement. There is no documentation for implementing any of this behavior beyond the protocol packet documentation. You can refer to the YML files [here](https://github.com/PrismarineJS/bedrock-protocol/blob/master/data/latest/proto.yml) and [here](https://github.com/PrismarineJS/bedrock-protocol/blob/master/data/latest/types.yaml) for information about packets with some notes on their usage. 

Providing a high level interface to Minecraft, such as handling movements or inventories is something that will in the future be handled by mineflayer, which already provides such an interface for Minecraft Java Edition. 

The examples folder contains some of the basic boilerplate you need to either start a server or create a client.

### How do I use x feature with the protocol library ?

We do not have all the answers you’re looking for. If we did, this behavior would probably already be implemented in a higher library. All of the libraries from the protocol lib to mineflayer are open source. You can (and are absolutely encouraged to!) help contribute to implement many of these high level features for others to use. 

### So how do you do anything ?!

Just send the right packets! There are many third party programs that work with Minecraft Bedrock Edition, such as PocketMine, DragonFly, GeyserMC, Nukkit, etc. which implement many of the functionality already and have done the bulk of the work for you. You can see what packets they send and the logic they implement and replicate it on-top of bedrock-protocol. If you know how to implement a specific feature, but need help with the bedrock-protocol side, you can [on the Discord](https://discord.com/invite/GsEFRM8). 

For implementing certain client-side logic, the easiest way is to use a proxy (see the Relay example) and observe packets as they are sent between the client and the server, and then replicate that functionality in code.

This debugging procedure generally consists of:
* Start a vanilla server
* Start a Relay with bedrock-protocol, and log all packets
* Connect with a vanilla client to the proxy
* Send a chat message that you're about to start a test
* Do the test with the client
* Send a chat message that you're done
* Disconnect the proxy, look at the packets between your start and stop message
* Replicate that logic in your code

You can then always PR a new feature you've implemented.

### It's too hard

It's always a great time to learn. Otherwise, you can track bedrock-protocol and other PrismarineJS projects for progress on mineflayer support for Bedrock.

* https://github.com/PrismarineJS/bedrock-protocol/issues/116